package systems.intelgeno.Horoscope;

public enum Seats {
	Air("Суудал:`ХИЙ` \n"
			+ "\nУрд зүгт гарч, нар зөв тойрч зүүн урд зүгээс ирнэ. "
						+ "\n\n«Ум базар падма мама бизъяа суухаа» тарнийг 7-21 удаа уншина. "
						+ "\n\nЗасал: Цаас урж хийсгээд чулуу, шороо хөдөлгөнө."),
	Fire("Суудал:`ГАЛ` \n"
			+ "\nЗүүн урагш гарч, урд зүгээc ирнэ."
			+"\n\n«Ум базар дагийни базар еэ мамаа бизъяа суухаа» тарнийг 7-21 удаа уншина. " 
			+"\n\nЗасал: Галаас гэмтэхгүй хэмээн сэтгэж гал гаргаж (Шүдэнз зурах), ус цасанд унтраана."),
	Wood("Суудал:`МОД` \n"
			+ "\nХойд зүгт гарч, зүүн зүгээс ирнэ."
			+"\n\n«Ум базар дагийний хум мама бизъяа суухаа» тарнийг 7-21 удаа уншина." 
			+"\n\nЗасал: Мод хугалж хаяна."),
	Mountain("Суудал:`УУЛ` \n"
			+ "\nБаруун хойд зүгт гарч зүүн хойд зүгээс ирнэ."
			+"\n\n«Ум будда дагийний хум мама бизъяа суухаа» тарнийг 7-21 удаа уншина."
			+"\n\nЗасал: Уул руу модны үртэс цацаж, шороо, чулуу хөдөлгөнө."),
	Steel ("Суудал:`ТӨМӨР` "
			+ "\n\nБаруун урд зүгт гарч, баруун зүгээс ирнэ."
		+"\n\n«Ум радна дагийний хум мама бизъяа суухаа» тарнийг 7-21 удаа уншина."
		+"\n\nЗасал: Багахан гал гаргаад төмөрт хүргэнэ. Төмрийн зүйл хаяна."),
	Space("Суудал:`ОГТОРГУЙ` "
			+ "\n\nЗүүн хойд зүгт гарч, нар зөв тойрч баруун хойд зүгээс ирнэ."
			+"\n\n«Ум базар пагмо хум мама бизъяа суухаа» тарнийг 7-21 удаа уншина." 
			+"\n\nЗасал: Агаарт цаас хийсгэн шороо чулуу хөдөлгөнө."),
	Water("Суудал:`УС` "
			+ "\n\nУрд зүгт гарч, хойд зүгээс ирнэ."
			+"\n\n«Ум гарма дагийни хум мама бизъяа суухаа» тарнийг 7-21 удаа уншина." 
			+"\n\nЗасал: Ус асгаж, дээгүүр нь алхана."),
	Earth("Суудал:`ШОРОО` "
			+ "\n\nБаруун хойт зүгт гарч, нар зөв тойрч баруун урд зүгээс ирнэ. "
			+ "\n\n«Ум бадма дагийни хум мама бизъяа суухаа» тарнийг 7-21 удаа уншина."
			+ "\n\nЗасал: Шороо, чулуунаас үл гэмтээнэ хэмээн сэтгэж шороо чулуу хөдөлгөнө."); 
	private String value;
	private Seats(String value) {
		this.setValue(value);
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
