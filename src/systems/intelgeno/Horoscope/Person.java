package systems.intelgeno.Horoscope;

import java.util.ArrayList;
import java.util.Date;

public class Person {

	private String gender;
	private Date birthDate;
//	private final static ArrayList<String> seats = new ArrayList<>();
//	private final static ArrayList<String> yearAnimal = new ArrayList<>();
	 
	
	public Person(Date birthDate, String gender) {
		this.setBirthDate(birthDate);
		this.setGender(gender);
		
//		seats.add("Хий");
//		seats.add("Мод");
//		seats.add("Уул");
//		seats.add("Ус");
//		seats.add("Огторгуй");
//		seats.add("Төмөр");
//		seats.add("Шороо");
//		seats.add("Гал");
//	
	
//	yearAnimal.add("Хулгана");
//	yearAnimal.add("Үхэр");
//	yearAnimal.add("Бар");
//	yearAnimal.add("Туулай");
//	yearAnimal.add("Луу");
//	yearAnimal.add("Могой");
//	yearAnimal.add("Морь");
//	yearAnimal.add("Хонь");
//	yearAnimal.add("Бич");
//	yearAnimal.add("Тахиа");
//	yearAnimal.add("Нохой");
//	yearAnimal.add("Гахай");
	}
	
	public Zodiac getZodiac(){
        Zodiac userZodiac = Zodiac.ARIES;
        for (Zodiac birthDateZ : Zodiac.values()) {
            if(birthDateZ.getStartDate().getMonth() == getBirthDate().getMonth()){
                if(birthDateZ.getStartDate().getDate() <= getBirthDate().getDate() ){
                    userZodiac = birthDateZ;
                    break;
                }
            }
            else if(birthDateZ.getEndDate().getMonth() == getBirthDate().getMonth()){
            	if(birthDateZ.getEndDate().getDate() >= getBirthDate().getDate()){
                    userZodiac = birthDateZ;
                    break;
                }
            }
        }
        return userZodiac;
    }
	
	public String getSeat(){
        if(getGender().equals("Эр")){
            if(getBirthDate().getYear()/8 == 1)
                return Seats.Air.getValue();
            else if(getBirthDate().getYear()/8 == 2)
                return Seats.Wood.getValue();
            else if(getBirthDate().getYear()/8 == 3)
                return Seats.Mountain.getValue();
            else if(getBirthDate().getYear()/8 == 4)
                return Seats.Water.getValue();
            else if(getBirthDate().getYear()/8 == 5)
                return Seats.Space.getValue();
            else if(getBirthDate().getYear()/8 == 6)
                return Seats.Steel.getValue();
            else if(getBirthDate().getYear()/8 == 7)
                return Seats.Earth.getValue();
            else 
                return Seats.Fire.getValue();
        }
        else{
            if(getBirthDate().getYear()/8 == 1)
                return Seats.Mountain.getValue();
            else if(getBirthDate().getYear()/8 == 2)
                return Seats.Wood.getValue();
            else if(getBirthDate().getYear()/8 == 3)
                return Seats.Air.getValue();
            else if(getBirthDate().getYear()/8 == 4)
                return Seats.Fire.getValue();
            else if(getBirthDate().getYear()/8 == 5)
                return Seats.Earth.getValue();
            else if(getBirthDate().getYear()/8 == 6)
                return Seats.Steel.getValue();
            else if(getBirthDate().getYear()/8 == 7)
                return Seats.Space.getValue();
            else
                return Seats.Water.getValue();
        }
    } 

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	@Override
	public String toString() {
		return "Person [gender=" + gender + ", birthDate=" + birthDate + "]";
	}

}
	
