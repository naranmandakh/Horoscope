package systems.intelgeno.Horoscope;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Date;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

public class HoroscopeMainUI {

	private JFrame frame;
	private JComboBox comboBox;
	Person person;
	private String zodiacImagePath = "";
	private String val;
	private String zurlaga;
	
	/**
	 * Launch the application.
	 * @throws IOException 
	 * @throws UnsupportedAudioFileException 
	 * @throws LineUnavailableException 
	 */
	public static void main(String[] args) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HoroscopeMainUI window = new HoroscopeMainUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public HoroscopeMainUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		zurlaga = "Хаврын амьсгал орсон өдөр болон тухайн жилийн амь бие эрхтэн махбодуудад дулдуйдан "
				+ "гал бич жилийн шороон үхрийг зураглавал:\n"
				+ "-Цагаан биетэй, улаан толгойтой, шар омруутай, эвэр, чих, сүүл нь улаан, улаан ходоодтой, шар шийртэй, амаа ангайгаад "
				+ "сүүлээ зүүн тийш шарвасан үхрийн өмнө шар биетэй, ногоон дээл өмсөж, цагаан бүс бүсэлсэн, "
				+ "үсээ толгой дээрээ тоорцоглон боосон хүү хоёр гутлаа бариад яаравчлан гүйж явна."
				+ "Жилийн ерөнхий байдал нь гандуу, хүчтэй аадар мөндөр ихтэй, халуун өвчин гарна. "
				+ "Жилийн сүүлээр ургац арвин авна. Мал сүрэгт ээлтэй боловч хямрал их болно. "
				+ "Залууст амгалан, нялхаст ширүүн, өвгөдөд ширүүн жил болно. Амгалан жаргалантай."
				+ "Муу нүүрт хэмээх гал бичин жилд үр тариа чухаг болно. ";
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBackground(new Color(0, 255, 255));
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setTitle("Сар шинийн зүг мөр гаргах зурхай");
		frame.setBounds(100, 100, 736, 693);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JComboBox PersonYear= new JComboBox();
		PersonYear.setBackground(new Color(255, 192, 203));
		PersonYear.setModel(new DefaultComboBoxModel(new String[] {"1900", "1901", "1902", "1903", "1904", "1905", "1906", "1907", "1908", "1909", "1910", "1911", "1912", "1913", "1914", "1915", "1916", "1917", "1918", "1919", "1920", "1921", "1922", "1923", "1924", "1925", "1926", "1927", "1928", "1929", "1930", "1931", "1932", "1933", "1934", "1935", "1936", "1937", "1938", "1939", "1940", "1941", "1942", "1943", "1944", "1945", "1946", "1947", "1948", "1949", "1950", "1951", "1952", "1953", "1954", "1955", "1956", "1957", "1958", "1959", "1960", "1961", "1962", "1963", "1964", "1965", "1966", "1967", "1968", "1969", "1970", "1971", "1972", "1973", "1974", "1975", "1976", "1977", "1978", "1979", "1980", "1981", "1982", "1983", "1984", "1985", "1986", "1987", "1988", "1989", "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "", "", ""}));
		PersonYear.setBounds(507, 36, 55, 20);
		frame.getContentPane().add(PersonYear);
		
		JComboBox PersonGender = new JComboBox();
		PersonGender.setBackground(new Color(0, 191, 255));
		PersonGender.setModel(new DefaultComboBoxModel(new String[] {"Эрэгтэй", "Эмэгтэй"}));
		PersonGender.setBounds(507, 95, 93, 20);
		frame.getContentPane().add(PersonGender);
		
		JLabel label = new JLabel("Жил");
		label.setBounds(507, 22, 37, 14);
		frame.getContentPane().add(label);
		
		JLabel lblNewLabel_2 = new JLabel("Хүйс");
		lblNewLabel_2.setBounds(507, 80, 46, 14);
		frame.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_1 = new JLabel("Сар");
		lblNewLabel_1.setBounds(572, 22, 46, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		JComboBox PersonMonth = new JComboBox();
		PersonMonth.setBackground(new Color(255, 192, 203));
		PersonMonth.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"}));
		PersonMonth.setBounds(572, 36, 46, 20);
		frame.getContentPane().add(PersonMonth);
		
		JLabel lblNewLabel_3 = new JLabel("Өдөр");
		lblNewLabel_3.setBounds(628, 22, 46, 14);
		frame.getContentPane().add(lblNewLabel_3);
		
		JComboBox PersonDay = new JComboBox();
		PersonDay.setBackground(new Color(255, 192, 203));
		PersonDay.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"}));
		PersonDay.setBounds(627, 36, 46, 20);
		frame.getContentPane().add(PersonDay);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(392, 39, 137, 159);
		textPane.setText(zurlaga);
		textPane.setLayout(new BoxLayout(textPane, BoxLayout.Y_AXIS));
		JPanel panel = new JPanel();
		panel.setLayout(new  BoxLayout(panel, BoxLayout.X_AXIS));
		JScrollPane scroll =  new JScrollPane(textPane);
		scroll.setBounds(165, 275, 540, 310);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		frame.getContentPane().add(scroll);
		
		JLabel label_1 = new JLabel("");
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setIcon(new ImageIcon(new ImageIcon(getClass().getResource("/cowImage.jpg")).getImage()));
		scroll.setColumnHeaderView(label_1);
		
		JLabel lblXvii = new JLabel("Энэ жилийн шороон үхрийн зурлага:");
		lblXvii.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblXvii.setHorizontalAlignment(SwingConstants.CENTER);
		lblXvii.setOpaque(true);
		lblXvii.setBackground(Color.LIGHT_GRAY);
		lblXvii.setBounds(165, 243, 540, 32);
		lblXvii.setText("XVII жарны \"Муур нүүрт\" хэмээх Гал бич жилийн шороон үхрийн зурлага");
		frame.getContentPane().add(lblXvii);
		
		JLabel lblNewLabel_4 = new JLabel("");
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblNewLabel_4.setBounds(10, 275, 156, 100);
		frame.getContentPane().add(lblNewLabel_4);
		
		JButton wayViewBtn = new JButton("Зүг мөр гаргах заавар");
		wayViewBtn.setBackground(Color.GREEN);
		wayViewBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Date personDate = new Date(Integer.parseInt(PersonYear.getSelectedItem().toString()),Integer.parseInt(PersonMonth.getSelectedItem().toString()),Integer.parseInt(PersonDay.getSelectedItem().toString()));  
				person = new Person(personDate, PersonGender.getSelectedItem().toString());
				textPane.setText(person.getSeat());
				lblXvii.setText("XVII жарны \"Муур нүүрт\" хэмээх Гал бич жилийн зүг мөрөө гаргах шинийн нэгний ёслол үйлдэх заавар");
				lblNewLabel_4.setText(person.getSeat().substring(0, person.getSeat().indexOf(" ")));
				scroll.getColumnHeader().setVisible(false);
			}
		});
		wayViewBtn.setBounds(223, 600, 184, 43);
		frame.getContentPane().add(wayViewBtn);
		
		JLabel mainMendchilgeLbl = new JLabel("");
		mainMendchilgeLbl.setBounds(165, 11, 318, 159);
		mainMendchilgeLbl.setIcon(new ImageIcon(new ImageIcon(getClass().getResource("/mendchilgee.jpg")).getImage()));
		frame.getContentPane().add(mainMendchilgeLbl);
		
		JLabel zodiacBackLabel = new JLabel();
		zodiacBackLabel.setBounds(10, 378, 156, 192);
		zodiacBackLabel.setIcon(new ImageIcon(new ImageIcon(getClass().getResource("/zodiac.jpg")).getImage()));
		frame.getContentPane().add(zodiacBackLabel);
		
		JButton zodiacInforBtn = new JButton("Ордны зурхай харах");
		zodiacInforBtn.setBackground(Color.GREEN);
		zodiacInforBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
//				Image[] imgZodiacs = {
//			            new ImageIcon(getClass().getResource("/asrlan.png")).getImage(),
//			            new ImageIcon(getClass().getResource("/bumba.png")).getImage(),
//			            new ImageIcon(getClass().getResource("/hilents.png")).getImage(),
//			            new ImageIcon(getClass().getResource("/honi.png")).getImage(),
//			            new ImageIcon(getClass().getResource("/iher.png")).getImage(),
//			            new ImageIcon(getClass().getResource("/jinluur.png")).getImage(),
//			            new ImageIcon(getClass().getResource("/matar.png")).getImage(),
//			            new ImageIcon(getClass().getResource("/melhii.png")).getImage(),
//			            new ImageIcon(getClass().getResource("/num.png")).getImage(),
//			            new ImageIcon(getClass().getResource("/ohin.png")).getImage(),
//			            new ImageIcon(getClass().getResource("/uher.png")).getImage(),
//			            new ImageIcon(getClass().getResource("/zagas.png")).getImage(),
//			        };
				Date personDate = new Date(Integer.parseInt(PersonYear.getSelectedItem().toString()),Integer.parseInt(PersonMonth.getSelectedItem().toString()),Integer.parseInt(PersonDay.getSelectedItem().toString()));
//				System.out.println(Integer.parseInt(PersonYear.getSelectedItem().toString())+"\t month: "+Integer.parseInt(PersonMonth.getSelectedItem().toString())+"\t day: "+Integer.parseInt(PersonDay.getSelectedItem().toString()));
				person = new Person(personDate, PersonGender.getSelectedItem().toString());
				textPane.setText(person.getZodiac().getValue());
				setVal(person.getZodiac().getValue().substring(0, person.getZodiac().getValue().indexOf(" ")).trim()); 
				
				switch (getVal()) {
				case "Хонь":
					setZodiacImagePath("/honi.jpg");
					break;
				case "Үхэр":
					setZodiacImagePath("/uher.jpg");
					break;
				case "Ихэр":
					setZodiacImagePath("/iher.jpg");
					break;
				case "Мэлхий":
					setZodiacImagePath("/melhii.jpg");
					break;
				case "Арслан":
					setZodiacImagePath("/arslan.jpg");
					break;
				case "Охин":
					System.out.println("Inner switch: "+getVal());
					setZodiacImagePath("/ohin.jpg");
					break;
				case "Жинлүүр":
					setZodiacImagePath("/jinluur.jpg");
					break;
				case "Хилэнц":
					setZodiacImagePath("/hilents.jpg");
					break;
				case "Нум":
					setZodiacImagePath("/num.jpg");
					break;
				case "Матар":
					setZodiacImagePath("/matar.jpg");
					break;
				case "Хумх":
					setZodiacImagePath("/bumba.jpg");
					break;
				case "Загас":
					setZodiacImagePath("/honi.jpg");
					break;
				default:
					setZodiacImagePath("/zodiac.jpg");
					break;
				}
				
				zodiacBackLabel.setIcon(new ImageIcon(new ImageIcon(getClass().getResource(getZodiacImagePath())).getImage()));
				lblXvii.setText("XVII жарны \"Муур нүүрт\" хэмээх Гал бич жилийн 12 ордны зурхай");
				lblNewLabel_4.setText(getVal());
				scroll.getColumnHeader().setVisible(false);
			}
		});
		zodiacInforBtn.setBounds(461, 600, 167, 43);
		frame.getContentPane().add(zodiacInforBtn);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBackground(Color.WHITE);
		lblNewLabel.setBounds(10, 11, 149, 159);
		lblNewLabel.setIcon(new ImageIcon(new ImageIcon(getClass().getResource("/monkey.jpg")).getImage()));
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblMendchilgeeT = new JLabel("17-р жарны Гал мичин жилийн сар шинийн мэнд дэвшүүлье\r\n");
		lblMendchilgeeT.setHorizontalAlignment(SwingConstants.CENTER);
		lblMendchilgeeT.setOpaque(true);
		lblMendchilgeeT.setBackground(new Color(255, 200, 0));
		lblMendchilgeeT.setFont(new Font("Serif", Font.PLAIN, 20));
		lblMendchilgeeT.setBounds(0, 200, 730, 32);
		frame.getContentPane().add(lblMendchilgeeT);
		
		JButton button = new JButton("Үндсэн хуудас");
		button.setBackground(new Color(0, 255, 0));
		button.setBounds(29, 600, 137, 43);
		frame.getContentPane().add(button);
		
		JLabel label_2 = new JLabel("<html>Та дээрхи хэсэгт өөрийн төрсөн он, сар, өдрийг оруулан энэ жилийн зүг мөр гаргах заавар болон зурхайгаа уншаарай</html>");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 10));
		label_2.setBackground(new Color(0, 255, 0));
		label_2.setOpaque(true);
		label_2.setHorizontalAlignment(SwingConstants.CENTER);
		label_2.setBounds(507, 146, 213, 43);
		frame.getContentPane().add(label_2);
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				scroll.getColumnHeader().setVisible(true);
				textPane.setText(zurlaga);
				lblNewLabel_4.setText("");
				lblXvii.setText("XVII жарны \"Муур нүүрт\" хэмээх Гал бич жилийн шороон үхрийн зурлага");
				zodiacBackLabel.setIcon(new ImageIcon(new ImageIcon(getClass().getResource("/zodiac.jpg")).getImage()));
			}
		});
		
	}

	public Color getComboBoxBackground() {
		return comboBox.getBackground();
	}

	public void setComboBoxBackground(Color background) {
		comboBox.setBackground(background);
	}

	public String getZodiacImagePath() {
		return zodiacImagePath;
	}

	public void setZodiacImagePath(String zodiacImagePath) {
		this.zodiacImagePath = zodiacImagePath;
	}

	public String getVal() {
		return val;
	}

	public void setVal(String val) {
		this.val = val;
	}
}
